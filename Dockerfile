FROM fedora:latest

RUN dnf -y update \
  && dnf clean all \
  && useradd slushy

USER slushy
